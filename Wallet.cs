﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.IO;
using System.Xml.Linq;

namespace Serijalizacija {
  public class Wallet {
    private List<CryptoCurrency> currencies;

    public Wallet() {
      currencies = new List<CryptoCurrency>();
    }

    public void AddCurrency(CryptoCurrency currency) {
      currencies.Add(currency);
      Serialize();
    }

    public IEnumerable<CryptoCurrency> GetCurrencies() {
      return
        from c in currencies
        orderby c.Name
        select c;
    }

        public void UpdateCurrency(CryptoCurrency old, CryptoCurrency replacement)
        {
            currencies.Remove(old);
            currencies.Add(replacement);
            Serialize();
        }

    public void RemoveCurrency(CryptoCurrency cc)
        {
            currencies.Remove(cc);
            Serialize();
        }

        // https://learn.microsoft.com/en-us/dotnet/standard/serialization/system-text-json/how-to?pivots=dotnet-8-0
        public void Serialize() {
            var jso = new JsonSerializerOptions();
            jso.WriteIndented = true;
            string json = JsonSerializer.Serialize(currencies,jso);
            File.WriteAllText("values.json", json);
        }

    public void Deserialize() {
            try { 
            string jsonContent = File.ReadAllText("values.json");
            currencies = JsonSerializer.Deserialize<List<CryptoCurrency>>(jsonContent);
            }
            catch { }
    }



    }
}
