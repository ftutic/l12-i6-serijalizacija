﻿namespace Serijalizacija
{
    partial class FormWallet
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            cbCurrencies = new ComboBox();
            lblBalance = new Label();
            scControls = new SplitContainer();
            btnAddCurrency = new Button();
            btnUpdateCurrency = new Button();
            ((System.ComponentModel.ISupportInitialize)scControls).BeginInit();
            scControls.Panel1.SuspendLayout();
            scControls.Panel2.SuspendLayout();
            scControls.SuspendLayout();
            SuspendLayout();
            // 
            // cbCurrencies
            // 
            cbCurrencies.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            cbCurrencies.FormattingEnabled = true;
            cbCurrencies.Location = new Point(12, 12);
            cbCurrencies.Name = "cbCurrencies";
            cbCurrencies.Size = new Size(678, 23);
            cbCurrencies.TabIndex = 0;
            cbCurrencies.SelectedValueChanged += cbCurrencies_SelectedValueChanged;
            // 
            // lblBalance
            // 
            lblBalance.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            lblBalance.Font = new Font("Segoe UI", 48F, FontStyle.Bold, GraphicsUnit.Point);
            lblBalance.Location = new Point(12, 61);
            lblBalance.Name = "lblBalance";
            lblBalance.Size = new Size(678, 196);
            lblBalance.TabIndex = 1;
            lblBalance.Text = "0.000000000 BTC";
            lblBalance.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // scControls
            // 
            scControls.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            scControls.Location = new Point(12, 306);
            scControls.Name = "scControls";
            // 
            // scControls.Panel1
            // 
            scControls.Panel1.Controls.Add(btnAddCurrency);
            // 
            // scControls.Panel2
            // 
            scControls.Panel2.Controls.Add(btnUpdateCurrency);
            scControls.Size = new Size(678, 56);
            scControls.SplitterDistance = 339;
            scControls.TabIndex = 2;
            // 
            // btnAddCurrency
            // 
            btnAddCurrency.Dock = DockStyle.Fill;
            btnAddCurrency.Location = new Point(0, 0);
            btnAddCurrency.Name = "btnAddCurrency";
            btnAddCurrency.Size = new Size(339, 56);
            btnAddCurrency.TabIndex = 0;
            btnAddCurrency.Text = "Unos valute";
            btnAddCurrency.UseVisualStyleBackColor = true;
            btnAddCurrency.Click += btnAddCurrency_Click;
            // 
            // btnUpdateCurrency
            // 
            btnUpdateCurrency.Dock = DockStyle.Fill;
            btnUpdateCurrency.Location = new Point(0, 0);
            btnUpdateCurrency.Name = "btnUpdateCurrency";
            btnUpdateCurrency.Size = new Size(335, 56);
            btnUpdateCurrency.TabIndex = 0;
            btnUpdateCurrency.Text = "Ažuriranje valute";
            btnUpdateCurrency.UseVisualStyleBackColor = true;
            btnUpdateCurrency.Click += btnUpdateCurrency_Click;
            // 
            // FormWallet
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(702, 374);
            Controls.Add(scControls);
            Controls.Add(lblBalance);
            Controls.Add(cbCurrencies);
            Name = "FormWallet";
            Text = "CryptoWallet";
            scControls.Panel1.ResumeLayout(false);
            scControls.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)scControls).EndInit();
            scControls.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private ComboBox cbCurrencies;
        private Label lblBalance;
        private SplitContainer scControls;
        private Button btnAddCurrency;
        private Button btnUpdateCurrency;
    }
}