namespace Serijalizacija
{
    public partial class FormWallet : Form
    {
        public Wallet wallet;

        public FormWallet()
        {
            InitializeComponent();
            wallet = new Wallet();
            wallet.Deserialize();
            updateCurrencyList();
        }

        public void updateCurrencyList()
        {
            cbCurrencies.Items.Clear();
            foreach (CryptoCurrency currency in wallet.GetCurrencies())
            {
                cbCurrencies.Items.Add(currency);
            }
        }

        private void btnAddCurrency_Click(object sender, EventArgs e)
        {
            FormCurrencyAdd formCurrencyAdd = new FormCurrencyAdd();
            if (formCurrencyAdd.ShowDialog() == DialogResult.OK)
            {
                CryptoCurrency? newCurrency = formCurrencyAdd.NewCurrency;
                if (newCurrency != null)
                {
                    if(newCurrency.Name != "") { 
                    wallet.AddCurrency(newCurrency);
                    updateCurrencyList();
                    cbCurrencies.SelectedItem = newCurrency;
                    }

                }
            }
        }

        private void cbCurrencies_SelectedValueChanged(object sender, EventArgs e)
        {
            CryptoCurrency cc = cbCurrencies.SelectedItem as CryptoCurrency;
            lblBalance.Text = cc.Value.ToString() + " " + cc.Currency.ToString();
        }

        private void btnUpdateCurrency_Click(object sender, EventArgs e)
        { 

            CryptoCurrency cc = cbCurrencies.SelectedItem as CryptoCurrency;
            if(cc != null) { 
            FormCurrencyAdd formCurrencyAdd = new FormCurrencyAdd(cc);
            if (formCurrencyAdd.ShowDialog() == DialogResult.OK)
            {
                CryptoCurrency? newCurrency = formCurrencyAdd.NewCurrency;
                if (newCurrency != null)
                {
                    if (newCurrency.Name != "")
                    {
                        wallet.UpdateCurrency(cc,newCurrency);
                        updateCurrencyList();

                        cbCurrencies.SelectedItem = newCurrency;
                        }

                }
            }
            }

        }
    }
}