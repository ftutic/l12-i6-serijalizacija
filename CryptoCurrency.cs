﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serijalizacija {
  [Serializable]
  public class CryptoCurrency {

        public CryptoCurrency() { 
            Name = string.Empty;
            Value = 0;
            Currency = string.Empty;
        }
    public CryptoCurrency(string name, decimal value, string currency)
        {
            Name = name;
            Value = value;
            Currency = currency;
        }

    public CryptoCurrency(string name, string currency, decimal value)
    {
            Name = name;
            Currency = currency;
            Value = value;
        }

        public override string ToString()
        {
            return Currency + " - " + Name;
        }

        public string Name { get; set; }
    public decimal Value { get; set; }
    public string Currency { get; set; }
  }
}
