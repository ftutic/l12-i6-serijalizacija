﻿using System;

namespace Serijalizacija
{
    public partial class FormCurrencyAdd : Form
    {
        public CryptoCurrency? NewCurrency { get; set; }
        public FormCurrencyAdd()
        {
            InitializeComponent();
            NewCurrency = new CryptoCurrency();
        }

        public FormCurrencyAdd(CryptoCurrency update)
        {
            InitializeComponent();
            this.Text = "Izmjena Valute";
            this.btnAdd.Text = "Izmjeni";
            NewCurrency = update;
            tbBalance.Text = update.Value.ToString();
            tbCurrencyName.Text = update.Name.ToString();
            tbCurrencyShortName.Text = update.Currency.ToString();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // stvoriti CryptoCurrency objekt na temelju textboxova
            
            NewCurrency.Name = tbCurrencyName.Text;
            NewCurrency.Value = Decimal.Parse(tbBalance.Text);
            NewCurrency.Currency = tbCurrencyShortName.Text;
            // ako je sve u redu, postaviti DialogResult na OK:
            DialogResult = DialogResult.OK;
            // zatvoriti formu
            Close();
        }
    }
}
